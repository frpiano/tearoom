/* Generated by AN DISI Unibo */ 
package it.unibo.clientsimulator

import it.unibo.kactor.*
import alice.tuprolog.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
	
class Clientsimulator ( name: String, scope: CoroutineScope  ) : ActorBasicFsm( name, scope ){

	override fun getInitialState() : String{
		return "s0"
	}
	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi			
	override fun getBody() : (ActorBasicFsm.() -> Unit){
		
				var ClientId	= 0
				var AssignedTable = 0
				var OrderName	= "caffe"
				//var TempResult = util.CheckTemperature.CHECK_POSITIVE.id
				var TempResult = domain.CheckTemperature.CHECK_POSITIVE.id
				//var TempResult = 30
		return { //this:ActionBasciFsm
				state("s0") { //this:State
					action { //it:State
						delay(3000) 
					}
					 transition( edgeName="goto",targetState="dorequest", cond=doswitch() )
				}	 
				state("dorequest") { //this:State
					action { //it:State
						request("notifyEntrance", "notifyEntrance(0)" ,"smartbell" )  
					}
					 transition(edgeName="t010",targetState="handleTempAnswer",cond=whenReply("tempTestResult"))
				}	 
				state("handleTempAnswer") { //this:State
					action { //it:State
						if( checkMsgContent( Term.createTerm("tempTestResult(TestAnswer)"), Term.createTerm("tempTestResult(TestAnswer)"), 
						                        currentMsg.msgContent()) ) { //set msgArgList
								 TempResult = payloadArg(0).toString().toInt()  
						}
					}
					 transition( edgeName="goto",targetState="waitToEnter", cond=doswitchGuarded({ TempResult == domain.CheckTemperature.CHECK_POSITIVE.id  
					}) )
					transition( edgeName="goto",targetState="endOfJob", cond=doswitchGuarded({! ( TempResult == domain.CheckTemperature.CHECK_POSITIVE.id  
					) }) )
				}	 
				state("waitToEnter") { //this:State
					action { //it:State
						println("clientsimulator | Waiting for table answer...")
					}
					 transition(edgeName="t011",targetState="handleEnteranswer",cond=whenDispatch("accept"))
					transition(edgeName="t012",targetState="waitForTable",cond=whenDispatch("inform"))
				}	 
				state("waitForTable") { //this:State
					action { //it:State
						if( checkMsgContent( Term.createTerm("inform(TimeToWait)"), Term.createTerm("inform(TimeToWait)"), 
						                        currentMsg.msgContent()) ) { //set msgArgList
								 val MinutesToWait = payloadArg(0).toInt()  
								println("clientsimulator | No tables available. Should wait for $MinutesToWait minutes")
						}
					}
					 transition(edgeName="t313",targetState="handleEnteranswer",cond=whenDispatch("accept"))
				}	 
				state("handleEnteranswer") { //this:State
					action { //it:State
						println("$name in ${currentState.stateName} | $currentMsg")
						if( checkMsgContent( Term.createTerm("accept(CID,TableNumber)"), Term.createTerm("accept(CID,TableNumber)"), 
						                        currentMsg.msgContent()) ) { //set msgArgList
								 
													ClientId = payloadArg(0).toInt()
													AssignedTable = payloadArg(1).toInt()
						}
						println("clientsimulator | at table $AssignedTable with ID = $ClientId")
					}
					 transition( edgeName="goto",targetState="dorequest", cond=doswitchGuarded({ ClientId < 1  
					}) )
					transition( edgeName="goto",targetState="think", cond=doswitchGuarded({! ( ClientId < 1  
					) }) )
				}	 
				state("think") { //this:State
					action { //it:State
						println("clientsimulator | Client with ID = $ClientId is thinking...")
						stateTimer = TimerActor("timer_think", 
							scope, context!!, "local_tout_clientsimulator_think", 30000.toLong() )
					}
					 transition(edgeName="t214",targetState="requestOrder",cond=whenTimeout("local_tout_clientsimulator_think"))   
				}	 
				state("requestOrder") { //this:State
					action { //it:State
						println("clientsimulator | Client with ID = $ClientId requesting $OrderName")
						forward("readyForOrder", "readyForOrder($ClientId,$AssignedTable)" ,"waiter" ) 
					}
					 transition(edgeName="t115",targetState="order",cond=whenRequest("takeOrder"))
				}	 
				state("order") { //this:State
					action { //it:State
						println("clientsimulator | Ordering $OrderName...")
						answer("takeOrder", "clientOrder", "clientOrder($OrderName,$ClientId,$AssignedTable)"   )  
					}
					 transition( edgeName="goto",targetState="waitOrder", cond=doswitch() )
				}	 
				state("waitOrder") { //this:State
					action { //it:State
						println("clientsimulator | Client with ID = $ClientId waiting for order for 30 secs")
						stateTimer = TimerActor("timer_waitOrder", 
							scope, context!!, "local_tout_clientsimulator_waitOrder", 40000.toLong() )
					}
					 transition(edgeName="t216",targetState="exit",cond=whenTimeout("local_tout_clientsimulator_waitOrder"))   
				}	 
				state("exit") { //this:State
					action { //it:State
						println("clientsimulator | Client with ID = $ClientId wants to exit!")
						forward("consumeFinished", "consumeFinished($ClientId,$AssignedTable)" ,"waiter" ) 
					}
					 transition(edgeName="t317",targetState="pay",cond=whenRequest("collectPayment"))
				}	 
				state("pay") { //this:State
					action { //it:State
						println("$name in ${currentState.stateName} | $currentMsg")
						if( checkMsgContent( Term.createTerm("collectPayment(MoneyValue)"), Term.createTerm("collectPayment(MoneyValue)"), 
						                        currentMsg.msgContent()) ) { //set msgArgList
								 val Fee = payloadArg(0).toInt();  
								println("clientsimulator | Client with ID = $ClientId PAYS $Fee")
								answer("collectPayment", "payment", "payment($Fee)"   )  
						}
					}
					 transition( edgeName="goto",targetState="endOfJob", cond=doswitch() )
				}	 
				state("endOfJob") { //this:State
					action { //it:State
						println("clientsimulator BYE")
					}
				}	 
			}
		}
}
