package test

import org.junit.Before
import org.junit.After
import org.junit.Test
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.delay
import it.unibo.kactor.ActorBasic
import it.unibo.kactor.QakContext
import it.unibo.kactor.MsgUtil
import it.unibo.kactor.MqttUtils
import it.unibo.barman.Barman
import org.eclipse.californium.core.CoapClient
import org.eclipse.californium.core.CoapResponse
import com.fasterxml.jackson.module.kotlin.*
import domain.SmartBellStatus
import domain.CheckTemperature
import domain.SmartBellState

class SmartBellTest {

	var barman: ActorBasic? = null
	var smartbell: ActorBasic? = null
	val mqttTest = MqttUtils("test")
	val initDelayTime = 4000L
	val useMqttInTest = false
	val mqttbrokerAddr = "tcp://localhost"
	val eventTopic = "unibo/polar"

	val context = "ctxtearoom"
	val destactor = "smartbell"
	val addr = "127.0.0.1:8010"
	val client = CoapClient()
	val uriStr = "coap://$addr/$context/$destactor"
	val mapper = jacksonObjectMapper()

	var tableNumber = 0;


	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	@Before
	fun systemSetUp() {

		kotlin.concurrent.thread(start = true) {

			client.uri = uriStr

			if (useMqttInTest) {
				while (!mqttTest.connectDone()) {
					println("	attempting MQTT-conn to ${mqttbrokerAddr}  for the test unit ... ")
					Thread.sleep(1000)
					mqttTest.connect("test_nat", mqttbrokerAddr)
				}
			}
			// chiamata bloccante fare tutti i setup delle connessioni prima 
			it.unibo.ctxtearoom.main()
		}
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	@After
	fun terminate() {
		println("testBarman terminated ")
	}

	fun checkResource(value: String) {

		if (smartbell != null) {
			val respGet: CoapResponse = client.get()
			val v = respGet.getResponseText()
			println("...checkResource |  $v value=$value ...")
			assertTrue(v == value)

		}
	}

	fun checkResource(value: ActorBasic?): String {
		if (value != null) {
			return value.geResourceRep()
		} else
			return "null"
	}

	fun getSmartBellStatus(): SmartBellStatus {
		println(checkResource(smartbell))
		return mapper.readValue<SmartBellStatus>(checkResource(smartbell));
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun verifyWaitForTime(doEnter: Boolean) {

		var sbStatus = getSmartBellStatus()
		while (sbStatus.timeToWait > 0 ||
			(sbStatus.timeToWait == 0 && sbStatus.smartBellState == SmartBellState.UPDATE_WAIT_TIME)) {

			println(" Client waiting.. ")
			delay(100)
			sbStatus = getSmartBellStatus()
		}
		// una volta scaduto il tempo o il cliente entra oppure se ne va
		if (doEnter) { // lo faccio entrare
			val enter = MsgUtil.buildDispatch("test", "enter", "enter(1,1)", "smartbell")
			if (useMqttInTest) {
				MsgUtil.sendMsg(enter, mqttTest)
			} else {
				MsgUtil.sendMsg(enter, smartbell!!)
			}
			delay(200)
			sbStatus = getSmartBellStatus()
			assert(sbStatus.smartBellState == SmartBellState.ENTER_CLIENT)
			delay(5100)
			sbStatus = getSmartBellStatus()
			assert(sbStatus.smartBellState == SmartBellState.WAITING_FOR_CLIENT)

		} else {
			// lascio che il cliente se ne vada
			val clientGone = MsgUtil.buildEvent("test", "clientGone", "clientGone(0)");
			if (useMqttInTest) {
				MsgUtil.sendMsg(clientGone, mqttTest, eventTopic)
			} else {
				MsgUtil.sendMsg(clientGone, smartbell!!)
			}
			delay(100)
			sbStatus = getSmartBellStatus()
			assert(sbStatus.smartBellState == SmartBellState.CLIENT_GONE)
			delay(100)
			sbStatus = getSmartBellStatus()
			assert(sbStatus.smartBellState == SmartBellState.WAITING_FOR_CLIENT)

		}
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun testSmartBellLocal(hasToWait: Boolean, hasToEnterAfterWait: Boolean = true) {
		println(" --- testSmartBellLocal ---")

		if (smartbell != null) {
			delay(150)

			// il cliente notifica la volontà di entrare
			val notifyEntrance = MsgUtil.buildRequest("test10", "notifyEntrance", "notifyEntrance(0)", "smartbell");

			if (useMqttInTest) {
				MsgUtil.sendMsg(notifyEntrance, mqttTest)
			} else {
				MsgUtil.sendMsg(
					notifyEntrance,
					smartbell!!
				)
			}

			delay(200)
			var sbStatus = getSmartBellStatus()

			// verifico di aver ottenuto una risposta dallo smartbell
			val tempChecked: CheckTemperature = sbStatus.checkTemperature;
			assertTrue(tempChecked == CheckTemperature.CHECK_POSITIVE || tempChecked == CheckTemperature.CHECK_NEGATIVE)

			delay(200)
			sbStatus = getSmartBellStatus()

			// se sono idoneo all'ingresso	
			if (tempChecked == CheckTemperature.CHECK_POSITIVE) {
				// viene richesto un tavolo
				assertTrue(sbStatus.smartBellState == SmartBellState.TABLE_REQUEST);
				delay(100)

				// se ho tavoli liberi faccio entrare il cliente
				if (!hasToWait) {
					val enter = MsgUtil.buildDispatch("test", "enter", "enter(1,1)", "smartbell")
					if (useMqttInTest) {
						MsgUtil.sendMsg(enter, mqttTest)
					} else {
						MsgUtil.sendMsg(enter, smartbell!!)
					}

					delay(200)
					sbStatus = getSmartBellStatus()
					assertTrue(sbStatus.smartBellState == SmartBellState.ENTER_CLIENT)

					assert(sbStatus.timeToWait == 0);
					delay(5100)
					sbStatus = getSmartBellStatus()
					// una volta fatto entrar il cliente mi rimetto in attesa di altri clienti
					assertTrue(sbStatus.smartBellState == SmartBellState.WAITING_FOR_CLIENT)
				} else {
					// se non ho tavoli liberi allora dico allo smartbell di emettere il tempo d'attesa
					val waitTime = 5L
					if (useMqttInTest) {
						MsgUtil.sendMsg("test", "timeToWait", "timeToWait(" + waitTime + ")", "smartbell", mqttTest)
					} else {
						MsgUtil.sendMsg(
							MsgUtil.buildDispatch("test", "timeToWait", "timeToWait(" + waitTime + ")", "smartbell"),
							smartbell!!
						)
					}

					delay(200)
					sbStatus = getSmartBellStatus()
					assertTrue(sbStatus.smartBellState == SmartBellState.WAIT_FOR_TIME)

					assert(sbStatus.timeToWait > 0);
					// verifico lo scorrere del tempo di attesa
					verifyWaitForTime(hasToEnterAfterWait)
				}


			} else if (tempChecked == CheckTemperature.CHECK_NEGATIVE) {

				assertTrue(sbStatus.smartBellState == SmartBellState.DENY_ENTRANCE);
			} else {
				println("tempChecked not valid")
				assert(false);
			}


		}
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	@Test
	fun testBarman() {
		runBlocking {

			//			while (waiter == null) {
//				delay(initDelayTime)
//				ctxExWaiter = it.unibo.kactor.sysUtil.getContext("ctxExWaiter");
//				waiter = ctxExWaiter!!.hasActor("waiter")
//			}
//			println("testBarman starts with waiter = $waiter")

			while (smartbell == null) {
				delay(initDelayTime)
				smartbell = it.unibo.kactor.sysUtil.getActor("smartbell")
			}
			println("testSmartBell starts with smartbell = $smartbell")

			delay(1000)

			// cliente che entra subito
			testSmartBellLocal(false)
			// cliente che aspetta e poi entra
			testSmartBellLocal(true, true)
			// cliente che aspetta e poi va via
			testSmartBellLocal(true, false)

			println("testSmartBell BYE")
		}
	}


}