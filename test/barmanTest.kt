package test

import org.junit.Before
import org.junit.After
import org.junit.Test
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.delay
import it.unibo.kactor.ActorBasic
import it.unibo.kactor.QakContext
import it.unibo.kactor.MsgUtil
import it.unibo.kactor.MqttUtils
import it.unibo.barman.Barman
import org.eclipse.californium.core.CoapClient
import org.eclipse.californium.core.CoapResponse

class BarmanTest {

	var barman: ActorBasic? = null
//	var waiter: ActorBasic? = null
//	var basicrobot: ActorBasic? = null
//	var  ctxExWaiter : QakContext? = null
	val mqttTest = MqttUtils("test")
	val initDelayTime = 4000L   // 
	val useMqttInTest = true
	val mqttbrokerAddr = "tcp://localhost"

	val context = "ctxtearoom"
	val destactor = "barman"
	val addr = "127.0.0.1:8010"
	val client = CoapClient()
	val uriStr = "coap://$addr/$context/$destactor"
	
	var tableNumber = 0;


	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	@Before
	fun systemSetUp() {

		kotlin.concurrent.thread(start = true) {
			println("testBarman systemSetUp start")
			client.uri = uriStr
			
			println("testBarman systemSetUp done")
			if (useMqttInTest) {
				while (!mqttTest.connectDone()) {
					println("	attempting MQTT-conn to ${mqttbrokerAddr}  for the test unit ... ")
					Thread.sleep(1000)
					mqttTest.connect("test_nat", mqttbrokerAddr)
				}
			}
			// chiamata bloccante fare tutti i setup delle connessioni prima 
			it.unibo.ctxtearoom.main()
		}
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	@After
	fun terminate() {
		println("testBarman terminated ")
	}

	fun checkResource(value: String) {

//		barman!!.geResourceRep()
		if (barman != null) {
			val respGet: CoapResponse = client.get()
			val v = respGet.getResponseText()
			println("...checkResource |  $v value=$value ...")
			assertTrue(v == value)

		}
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun testBarmanLocal() {
		println(" --- testBarmanLocal ---")

		if (barman != null) {
			delay(150)
			println(" 1. test waitingForOrder ")
			checkResource("waitingForOrder")

			if (useMqttInTest) {
				MsgUtil.sendMsg("test", "order", "order(caffe, " + (tableNumber % 2 + 1) + ", " + (tableNumber % 2 + 1) + ")", "barman", mqttTest)
			} else {
				MsgUtil.sendMsg("test", "order", "order(caffe, " + (tableNumber % 2 + 1) + "," + (tableNumber % 2 + 1) + ")", barman!!)
			}

			delay(500)
			checkResource("prepareOrder(caffe, " + (tableNumber % 2 + 1) + ", " + (tableNumber % 2 + 1) + ")")
			delay(12000)
			checkResource("orderCompleted(caffe, " + (tableNumber % 2 + 1) + ", " + (tableNumber % 2 + 1) + ")")

			tableNumber++;
		}
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	@Test
	fun testBarman() {
		runBlocking {
			
//			while (waiter == null) {
//				delay(initDelayTime)
//				ctxExWaiter = it.unibo.kactor.sysUtil.getContext("ctxExWaiter");
//				waiter = ctxExWaiter!!.hasActor("waiter")
//			}
//			println("testBarman starts with waiter = $waiter")
			
			while (barman == null) {
				delay(initDelayTime)
				barman = it.unibo.kactor.sysUtil.getActor("barman")
			}
			println("testBarman starts with barman = $barman")
			
			delay(1000)

			testBarmanLocal()
			
			//delay(30000)
			//testBarmanLocal()
			
			println("testBarman BYE with barman in  ")
		}
	}


}