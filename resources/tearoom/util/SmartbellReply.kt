package tearoom.util

data class SmartbellReply(val checkTemperature: String, val receivedClientId: Int, val receivedTableId: Int, val clientWaiting: Boolean) { }