package util

import java.util.*
import com.fasterxml.jackson.module.kotlin.*
import java.lang.IllegalStateException
import com.fasterxml.jackson.annotation.JsonValue

//https://bezkoder.com/kotlin-convert-json-to-object-jackson/
/*
enum class CheckTemperature(val id: Int, @get:JsonValue val text: String) {
	CHECK_POSITIVE(1, "Temperature is in an acceptable range"),
	CHECK_NEGATIVE(2, "Temperature is not in an acceptable range"),
	NONE(3, "No test done");

	override fun toString(): String {
		return "${super.toString()}($id,$text)"
	}

}

enum class TableState(val id: Int, @get:JsonValue val value: String) {
	CLEAN(1, "Clean"),
	DIRTY(2, "Dirty"),
	BUSY(3, "Busy"),
	// non dovrebbe mai averlo, inserito per fare controlli di robustezza
	UNKNOWN(4, "Unknown");

	override fun toString(): String {
		return "${super.toString()}($id,$value)"
	}
}

enum class WaiterState(val id: Int, @get:JsonValue val value: String) {
	AT_HOME(1, "At home"),
	SERVING(2, "Serving"),
	MOVING_TO(3, "Moving to"),
	AT_POSITION(4, "At position"),
	CLEANING(5, "Cleaning"),
	COLLECTING(5, "Collecting")
}

enum class OrderState(val id: Int, @get:JsonValue val value: String) {
	SUBMIT(1, "Submit"),
	PREPARE(2, "Prepare"),
	READY(3, "Ready"),
	COMPLETE(4, "Completed")
}

class CurrentState() {
	val tableMap = hashMapOf<Int, TableState>()
	val orderMap = hashMapOf<Int, OrderState>()
	var targetTable: Int
	var targetOrder: Int
	var waiterState: WaiterState
	var waiterPosX: Int
	var waiterPosY: Int

	init {
		targetTable = 0
		targetOrder = 0
		waiterState = WaiterState.AT_HOME
		waiterPosX = 0
		waiterPosY = 0
	}

	fun insertTableState(key: Int, value: TableState) {
		tableMap[key] = value
	}

	fun insertTableStateFromString(key: Int, value: String) {
		try {
			val tableState = TableState.valueOf(value.toUpperCase());
			tableMap[key] = tableState
		} catch (ise: IllegalStateException) {
			tableMap[key] = TableState.UNKNOWN
		}
	}

	fun getAvalaibleTable(): Int {
		var counter: Int = 0;
		enumValues<TableState>().forEach {
			if (it == TableState.CLEAN) {
				counter++
			}
		}
		return counter
	}

	fun getTableState(key: Int): TableState {
		return tableMap[key]!!
	}

	fun insertOrderState(key: Int, value: OrderState) {
		orderMap[key] = value
	}

	fun getOrderState(key: Int): OrderState {
		return orderMap[key]!!
	}
}

object JSONUtils {

	private val mapper = jacksonObjectMapper()

	fun toJSON(obj: Any): String {

		return this.toJSON(obj, false)

	}

	fun toJSON(obj: Any, withPretty: Boolean): String {

		if (withPretty) {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} else {
			return mapper.writeValueAsString(obj);
		}

	}
}

class SmartBellStatus() {
	var checkTemperature: CheckTemperature
	var timeToWait: Int
	var receivedClientId : Int
	var receivedTableId : Int
	var clientWaiting : Boolean

	init {
		checkTemperature = CheckTemperature.NONE
		timeToWait = 0
		receivedClientId = 0
		receivedTableId = 0
		clientWaiting = false
	}
	
	fun reset() {
		checkTemperature = CheckTemperature.NONE
		timeToWait = 0
		receivedClientId = 0
		receivedTableId = 0
	}

	fun decTimeToWait(){
		timeToWait--
	}
	
	fun resetTimeToWait(){
		timeToWait = 0
	}
}


fun main() {

	// esempi
	var ct: CheckTemperature = CheckTemperature.CHECK_POSITIVE

	var currState = CurrentState()

	currState.insertTableStateFromString(1, "clean");


	currState.insertTableState(1, TableState.CLEAN)
	currState.insertTableState(2, TableState.CLEAN)

	currState.waiterState = util.WaiterState.AT_HOME

	currState.insertOrderState(1, OrderState.READY)
	currState.insertOrderState(2, OrderState.PREPARE)

	val mapper = jacksonObjectMapper()
	
	var sbt : SmartBellStatus

//	var jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(currState);

	var jsonString = JSONUtils.toJSON(currState)

	println("json value of currentState: $jsonString")

//	var id = ct.id
//	var text = ct.text
//	println("check is: " + ct.id)
//
//	var res = 1 % 2;
//	println(res);
//	res = 2 % 2;
//	println(res);
}*/