package coap

import org.eclipse.californium.core.CoapClient
import org.eclipse.californium.core.CoapResponse
import org.eclipse.californium.core.coap.MediaTypeRegistry
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.delay
import it.unibo.kactor.MsgUtil
import it.unibo.kactor.ApplMessage
import java.util.Scanner
import org.eclipse.californium.core.CoapHandler 
 
object tearoomCoapObserver {

    private val clientBarman = CoapClient()
	
	private val ipaddr      = "127.0.0.1:8010"		//5683 default
	private val context     = "ctxtearoom"
 	private val actorBarman   = "barman"
//	private val ipaddr      = "localhost:8020"		//5683 default
//	private val context     = "ctxbasicrobot"
// 	private val destactor   = "basicrobot"
 

	fun init(){
       val uriStr = "coap://$ipaddr/$context/$actorBarman"
	   println("tearoomCoapObserver | START uriStr: $uriStr")
       clientBarman.uri = uriStr
       clientBarman.observe(object : CoapHandler {
            override fun onLoad(response: CoapResponse) {
                println("barmanCoapObserver | GET RESP-CODE= " + response.code + " content:" + response.responseText)
            }
            override fun onError() {
                println("barmanCoapObserver | FAILED")
            }
        })		
	}

 }

 
 fun main( ) {
		tearoomCoapObserver.init()
		System.`in`.read()   //to avoid exit
 }

