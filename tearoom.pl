%====================================================================================
% tearoom description   
%====================================================================================
mqttBroker("localhost", "1883", "unibo/polar").
context(ctxtearoom, "127.0.0.1",  "TCP", "8010").
context(ctxexwaiter, "localhost",  "TCP", "8060").
 qactor( waiter, ctxexwaiter, "external").
  qactor( barman, ctxtearoom, "it.unibo.barman.Barman").
  qactor( smartbell, ctxtearoom, "it.unibo.smartbell.Smartbell").
